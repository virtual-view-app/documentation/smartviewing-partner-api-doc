# Smart Viewing Partner Api Documentation

Smart Viewing Partner API helps you to give your customers access to Smart Viewing products in your own Software or Portal.

## Getting Started

A swagger documentation is available for you to understand, test and easily implement the API.

https://app.swaggerhub.com/apis/virtual-view-app/smartviewing-partner-api/1.0.1

### Prerequisites
Interested in testing Smart Viewing Partner API, contact us at info@smartviewing.com to get your credentials.

### Environments

#### Production Endpoint
https://api.smartviewing.com/partner/v1

#### Sandbox Endpoint
https://api.staging.smartviewing.com/partner/v1

### Rest
The Smart Viewing Partner API is organised around REST.<br />
The API attempts to use predictable, resource-oriented URLs and to use HTTP status codes to indicate errors.

### Https
The Smart Viewing Partner API requires all communications to be secured using TLS 1.2 or greater.

### Dates
All representations of dates are strings in [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html) format.<br />
You can provide date strings that are either UTC (for example, 2021-01-01T13:25:00Z) or offset from UTC to indicate time zone (for example, 2021-01-01T23:25:00+10:00 for AEST - ten hours ahead of UTC).

### Authentication

#### Basic authentication

Use a basic authorization header to verify your identity

Manually add the authorization header
```
Authorization: Basic Base64Encode(`{API_KEY}:{API_SECRET}`)
```

Curl example
```
curl --user API_PROD_KEY:API_PROD_SECRET --request GET 'https://api.smartviewing.com/partner/v1/account' 
curl --user API_TEST_KEY:API_TEST_SECRET --request GET 'https://api.staging.smartviewing.com/partner/v1/account' 
```

#### HMAC signature authentication

Instead of token-based authorization, it's possible to use HMAC credentials.<br />
These credentials are used to create an authorization header.<br />
Calculating signatures provides identity verification and in-transit data integrity.<br />
Each signature is tied to the time stamp of the request, so you can't reuse authorization headers after 15 minutes.<br />
The header is composed of four components: an algorithm declaration, credential information, signed headers, and the calculated signature.

##### Creating an authorization header
First, we need to create a request in a standardized format.
1.  Declare which HTTP method we are using (for example, POST)
2.  Define the resource that we are accessing in a standardized fashion.<br />
This is the part of the address in between https://endpoint and the query string. For example /viewings.

3. Calculate the current timestamp in millisecond

4. Create the header signature<br />
Combine the access key, HTTP method, resource path and the timestamp using semicolon and hash the value by using the SHA-256 algorithm and your secret key.
```
sha256(`${access_key};${http_method};${resource_path};${timestamp}`)
```

5. Create the content signature<br />
Combine the query string with the raw body content and hash by using the SHA-256 algorithm and your secret key.<br />
If the request has query string parameters, they must be standardized by being percent-encoded (for example, spaces are be represented as %20) before hashing.<br />

```
content_to_sign = sha256(`{query_string_without_questionmark}{raw_body}`);
```

Now the only step left is assembling the authorization header as shown:

1. Add the Authorization header
```
Authorization: SV1-HMAC-SHA256 Base64(AccessKey={AccessKey},HeaderSignature={HeaderSignature},ContentSignature={ContentSignature})
```

2. Add the X-Authorization-Timestamp header<br />
The pre-calculated timestamp must be added in a separate header named X-Authorization-Timestamp. The value could be in millisecond or second.
```
X-Authorization-Timestamp: 1586945556963
```

##### Generating an authorization header
**Postman Example**

Download the [postman collection](clients/postman/postman_collection.json) to quickly test the API.<br/>
You need to setup the 3 following environment variables:<br/>
partnerApiKey = YOUR_ACCESS_KEY<br/>
partnerApiSecret = YOUR_SECRET_KEY<br/>
partnerApiUrl = https://api.staging.smartviewing.com/partner/v1<br/>
Plese check out the Postman pre-script in the collection to debug the Authorization header calculation.<br />

**Python Example**
[clients/python/authentication.py](clients/python/authentication.py)

## Built With
* [Typescript](https://www.typescriptlang.org/) / [Python](https://www.python.org/) - Our main development languages
* [Jenkins](https://jenkins.io/) - Continuous Integration and Continuous Delivery
* [Terraform](https://www.terraform.io/) - Infrastructure provisioning
* [Kubernetes](https://kubernetes.io/) - Containers deployment, scaling, and management
* [Amazon Web Services](https://aws.amazon.com/) - Used for Infrastructure Hosting
* [Microsoft Azure](https://azure.microsoft.com/) - Used for Real Time Disaster Recovery Strategy
