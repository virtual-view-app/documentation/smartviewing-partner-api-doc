import os
import datetime
import hashlib
import hmac
import requests
import json
import base64

from dotenv import load_dotenv
load_dotenv()

# please don't store credentials directly in code
access_key = os.environ.get('API_ACCESS_KEY')
secret_key = os.environ.get('API_SECRET_KEY')
endpoint = os.environ.get('API_ENDPOINT')
http_method = 'POST'
resource_path = '/viewings'
query_string = ''

# payload example
data_payload = {'example' : 'data'}

# hashing
def hash(content, secret):
	return hmac.new(secret.encode('utf-8'), msg=content.encode('utf-8'), digestmod=hashlib.sha256).hexdigest()

timestamp = datetime.datetime.now().strftime("%s")

# preparing headers signatures : header and content
header_scope = '{};{};{};{}'.format(access_key, http_method, resource_path, timestamp)
header_signature = hash(header_scope, secret_key)


# note : add the query string without the questionmark and the body in raw string format
content = query_string + json.dumps(data_payload)
content_signature = hash(content, secret_key)

# assemble all elements into the 'authorization' header
authorization_scope = 'AccessKey={},HeaderSignature={},ContentSignature={}'.format(access_key, header_signature, content_signature)
authorization = base64.b64encode(authorization_scope.encode('utf-8'))

# assemble the headers 
hashing_algorithm = 'SV1-HMAC-SHA256'
signature_header = '{} {}'.format(hashing_algorithm, authorization.decode('utf-8'))
headers = {'Authorization': signature_header, 'X-Authorization-Timestamp': timestamp}


# create and send the request
request_url = '{}{}'.format(endpoint, resource_path)
print('\nSending `%s` request -----------------------' % http_method)
print('Request URL = {}'.format(request_url))

request = requests.request(method=http_method, url=request_url, json=data_payload, headers=headers)
print('\nResponse  ----------------------------------')
print('Response code: %d\n' % request.status_code)

print(json.dumps(request.json(), indent=4))
